# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require utf8-locale

myexparam meson_minimum_version=0.41.0

export_exlib_phases pkg_setup src_unpack src_prepare src_configure src_compile src_test src_install

DEPENDENCIES="
    build:
        sys-devel/meson[>=$(exparam meson_minimum_version)]
        sys-devel/ninja
"

MESON_SOURCE="${MESON_SOURCE:-${WORK}}"
WORK="${WORKBASE}/_build"

meson_pkg_setup() {
    require_utf8_locale
}

meson_src_unpack() {
    default
    if [[ $(type -t scm_src_unpack) == function ]]; then
        scm_src_unpack
    fi
    edo mkdir -p "${WORK}"
}

meson_src_prepare() {
    edo cd "${MESON_SOURCE}"
    default
}

meson_switch() {
    illegal_in_global_scope

    # <option name> [<flag name> [<value if enabled> [<value if disbaled>]]]
    echo "-D${2:-$(optionfmt ${1})}=$(option "${1}" "${3:-true}" "${4:-false}")"
}

meson_enable() {
    illegal_in_global_scope

    # <option name> [<flag name> [<value if enabled> [<value if disbaled>]]]
    echo "-Denable-${2:-$(optionfmt ${1})}=$(option "${1}" "${3:-true}" "${4:-false}")"
}

exmeson() {
    local target=$(exhost --target)

    # Specifying CC and CXX is only necessary for cross-compiling, but
    # because build=target for native builds I omit the conditional.
    CC=$(exhost --build)-cc CXX=$(exhost --build)-c++             \
    edo meson                                                     \
        --prefix=/usr                                             \
        --bindir=/usr/${target}/bin                               \
        --libdir=/usr/${target}/lib                               \
        --libexecdir=/usr/${target}/libexec                       \
        --includedir=/usr/${target}/include                       \
        --sysconfdir=/etc                                         \
        --datadir=/usr/share                                      \
        --mandir=/usr/share/man                                   \
        --default-library=shared                                  \
        $(exhost --is-native -q || echo "--cross-file /usr/share/meson/$(exhost --target).txt" ) \
        "${@}"
}

meson_src_configure() {
    exmeson \
        "${MESON_SRC_CONFIGURE_PARAMS[@]}"                        \
        $(for s in "${MESON_SRC_CONFIGURE_OPTION_SWITCHES[@]}"; do
            meson_switch ${s}
        done)                                                     \
        $(for s in "${MESON_SRC_CONFIGURE_OPTION_ENABLES[@]}"; do
            meson_enable ${s}
        done)                                                     \
        $(for s in "${MESON_SRC_CONFIGURE_OPTIONS[@]}"; do
            option ${s}
        done)                                                     \
        $(for s in "${MESON_SRC_CONFIGURE_TESTS[@]}"; do
            expecting_tests ${s}
        done)                                                     \
        ${MESON_SOURCE}
}

meson_src_compile() {
    edo ninja -v -j${EXJOBS:-1}
}

meson_src_test() {
    edo ninja -v -j${EXJOBS:-1} test
}

meson_src_install() {
    DESTDIR="${IMAGE}" edo ninja install
    cd "${MESON_SOURCE}"
    emagicdocs
}

