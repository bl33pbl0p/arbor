# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.bz2 ]

SUMMARY="Stream-oriented XML parser written in C"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~x86"
MYOPTIONS="
    ( parts: binaries development documentation libraries )
    ( libc: glibc ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build+run:
        libc:glibc? ( sys-libs/glibc[>=2.25] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --with-xmlwf
    # Don't rebuild xmlwf man page, would need docbook-utils
    --without-docbook
    # Would use libbsd for arc4random_buf, but we can just use getrandom from
    # Linux 3.17+ with glibc[>=2.25].
    --without-libbsd
)

src_install() {
    default

    expart binaries /usr/$(exhost --target)/bin
    expart development /usr/$(exhost --target)/include
    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
}

